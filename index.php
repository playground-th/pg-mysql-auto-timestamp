<?php

if(isset($_GET['btnSubmit']))
{
    $database_name = $_GET['database_name'];
    $table_name = $_GET['table_name'];

    echo "<pre>";
    echo "

    USE `$database_name`;

    ALTER TABLE `$database_name`.`$table_name`
    ADD COLUMN `created_at` DATETIME NULL DEFAULT '0000-00-00 00:00:00',
    ADD COLUMN `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`;

    UPDATE `$database_name`.`$table_name` SET `created_at`= NOW() WHERE 1=1;

    ALTER TABLE `$database_name`.`$table_name`
    CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`;

    DELIMITER $$

    DROP TRIGGER IF EXISTS $database_name.".$table_name."_BEFORE_INSERT$$
    USE `$database_name`$$
    CREATE DEFINER = CURRENT_USER TRIGGER `$database_name`.`".$table_name."_BEFORE_INSERT` BEFORE INSERT ON `$table_name` FOR EACH ROW
    BEGIN
    	SET NEW.`created_at` = NOW();
    END$$
    DELIMITER ;

    ";
    echo "</pre>";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <hr>
        <form class="" action="index.php" method="get">
            Database Name : <input type="text" name="database_name" value="<?php echo $database_name ?>">
            Table Name : <input type="text" name="table_name" value="<?php echo $table_name ?>">
            <input type="submit" name="btnSubmit" value="Generate">
        </form>
    </body>
</html>
